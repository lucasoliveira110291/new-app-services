﻿using System;

namespace Aplicacao.Core.Model
{
    public class Produto
    {
        #region Private

        private int id = 0;
        private string nome = String.Empty;

        #endregion Private

        #region Public

        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Nome
        {
            get { return this.nome; }
            set { this.nome = value; }
        }

        #endregion Public
    }
}
