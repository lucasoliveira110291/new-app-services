﻿using System;

namespace Aplicacao.Core.Model
{
    public class Usuario
    {
        #region Private

        private string id = String.Empty;
        private string senha = String.Empty;

        #endregion Private

        #region Public

        public string Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public string Senha
        {
            get { return this.senha; }
            set { this.senha = value; }
        }

        #endregion Public
    }
}
