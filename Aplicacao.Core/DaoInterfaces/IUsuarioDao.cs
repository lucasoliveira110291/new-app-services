﻿using Aplicacao.Core.Model;

namespace Aplicacao.Core.DaoInterfaces
{
    public interface IUsuarioDao
    {
        Usuario SelectUsuarioByIdPass(
            string id,
            string senha);
    }
}
