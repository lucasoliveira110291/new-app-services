﻿using Aplicacao.Core.Model;
using System.Collections.Generic;

namespace Aplicacao.Core.DaoInterfaces
{
    public interface IProdutoDao
    {
        List<Produto> SelectAllProdutos();
    }
}
