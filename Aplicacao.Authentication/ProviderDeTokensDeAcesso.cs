﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using aplicacao.Core.Negocios.Bussiness;
using Aplicacao.Core.Model;

namespace ExemploAccessToken
{
    public class ProviderDeTokensDeAcesso : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication
            (OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials
            (OAuthGrantResourceOwnerCredentialsContext context)
        {
            UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
            Usuario usuario = new Usuario();

            // encontrando o usuário
            usuario = usuarioBusiness.SelectUsuarioByIdPass(
                    context.UserName,
                    context.Password);

            // cancelando a emissão do token se o usuário não for encontrado
            if (usuario == null)
            {
                context.SetError("invalid_grant",
                    "Usuário não encontrado um senha incorreta.");
                return;
            }

            // emitindo o token com informacoes extras
            // se o usuário existe
            var identidadeUsuario = new ClaimsIdentity(context.Options.AuthenticationType);
            context.Validated(identidadeUsuario);
        }

        
    }
}