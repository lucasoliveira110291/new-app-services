﻿using MySql.Data.MySqlClient;

namespace Aplicacao.Data.Conexao
{
    public class Conexao
    {
        public MySqlConnection Conectar()
        {
            var builder = new MySqlConnectionStringBuilder
            {
                Server = "184.173.51.26",
                Database = "performance",
                UserID = "root",
                Password = "HEfc0xz0Eq",
                SslMode = MySqlSslMode.None,
                Port = 3306,
            };

            return new MySqlConnection(builder.ConnectionString);
        }        
    }
}
