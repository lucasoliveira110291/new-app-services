﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Aplicacao.Core.DaoInterfaces;
using Aplicacao.Core.Model;

namespace Aplicacao.Data.Dao
{
    public class ProdutoDao : IProdutoDao
    {
        public List<Produto> SelectAllProdutos()
        {
            List<Produto> listProduto = new List<Produto>();
            Produto produto = null;
            Conexao.Conexao conexao = new Conexao.Conexao();

            MySqlConnection conn = conexao.Conectar();
            conn.Open();

            string query =
                "SELECT " +
                    "PROORD, " +
                    "PRONOM, " +
                    "APPFLG, " +
                    "PROPRDDAT, " +
                    "TOTFLG, " +
                    "SGUFLG, " +
                    "PRODESDAT " +
                "FROM " +
                    "pro";

            var command = conn.CreateCommand();
            command.CommandText = query;

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                produto = new Produto();
                produto.Id = Convert.ToInt32(reader["PROORD"].ToString());
                produto.Nome = reader["PRONOM"].ToString();

                listProduto.Add(produto);
            }

            return listProduto;
        }
    }
}
