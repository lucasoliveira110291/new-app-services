﻿using Aplicacao.Core.DaoInterfaces;
using Aplicacao.Core.Model;
using MySql.Data.MySqlClient;

namespace Aplicacao.Data.Dao
{
    public class UsuarioDao : IUsuarioDao
    {
        public Usuario SelectUsuarioByIdPass(
            string id, 
            string senha)
        {            
            Usuario usuario = null;
            Conexao.Conexao conexao = new Conexao.Conexao();

            MySqlConnection conn = conexao.Conectar();
            conn.Open();

            string query =
                "SELECT " +
                    " USRMAT, " +
                    " USRSEN " +
                " FROM " +
                    " usr " +
                " WHERE " +
                    " USRMAT = '" + id +"'" +
                    " AND USRSEN = '" + senha + "'";

            var command = conn.CreateCommand();
            command.CommandText = query;

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                usuario = new Usuario();
                usuario.Id = reader["USRMAT"].ToString();
                usuario.Senha = reader["USRSEN"].ToString();
            }

            return usuario;
        }
    }
}
