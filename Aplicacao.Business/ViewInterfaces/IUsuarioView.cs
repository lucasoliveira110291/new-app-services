﻿using Aplicacao.Core.Model;

namespace aplicacao.Core.Negocios.BusinessInterfaces
{
    public interface IUsuarioView
    {
        Usuario SelectUsuarioByIdPass(
            string id,
            string senha);
    }
}
