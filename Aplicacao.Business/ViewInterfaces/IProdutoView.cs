﻿using Aplicacao.Core.Model;
using System.Collections.Generic;

namespace Aplicacao.Business.ViewInterfaces
{
    public interface IProdutoView
    {
        List<Produto> SelectAllProdutos();
    }
}
