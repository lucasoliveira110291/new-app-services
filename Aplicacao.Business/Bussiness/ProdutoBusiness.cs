﻿using Aplicacao.Business.ViewInterfaces;
using Aplicacao.Core.Model;
using Aplicacao.Data.Dao;
using System.Collections.Generic;

namespace Aplicacao.Business.Bussiness
{

    public class ProdutoBusiness : IProdutoView
    {
        #region Dao

        private ProdutoDao produtoDao = new ProdutoDao();

        #endregion

        public List<Produto> SelectAllProdutos()
        {
            List<Produto> results = new List<Produto>();

            try
            {
                results = produtoDao.SelectAllProdutos();

                return results;
            }
            catch
            {
                throw;
            }
        }
    }
}
