﻿using aplicacao.Core.Negocios.BusinessInterfaces;
using Aplicacao.Core.Model;
using Aplicacao.Data.Dao;

namespace aplicacao.Core.Negocios.Bussiness
{
    public class UsuarioBusiness : IUsuarioView
    {

        #region Dao

        private UsuarioDao produtoDao = new UsuarioDao();

        #endregion

        public Usuario SelectUsuarioByIdPass(
            string id,
            string senha)
        {
            Usuario results = new Usuario();

            try
            {
                results = produtoDao.SelectUsuarioByIdPass(
                    id,
                    senha);

                return results;
            }
            catch
            {
                throw;
            }
        }
    }
}
